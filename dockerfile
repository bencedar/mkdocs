FROM python:3.8-buster

RUN apt-get update && apt-get install -y \
  build-essential \
  python3-dev \
  python3-pip \
  python3-setuptools \
  python3-wheel \
  python3-cffi \
  libcairo2 \
  libpango-1.0-0 \
  libpangocairo-1.0-0 \
  libgdk-pixbuf2.0-0 \
  libffi-dev \
  shared-mime-info \
  && apt-get clean all \
  && rm -rf /var/lib/apt/lists/* \
  && pip install --no-cache-dir WeasyPrint \
  && rm -rf ~./.cache/pip/*

RUN pip install --no-cache-dir \
  mkdocs \
  Pygments \
  markdown \
  pymdown-extensions \
  mkdocs-material-extensions \
  mkdocs-material \
  mkdocs-minify-plugin \
  mkdocs-git-revision-date-localized-plugin \
  mkdocs-awesome-pages-plugin \
  mkdocs-with-pdf \
  && rm -rf ~./.cache/pip/*

WORKDIR /docs

EXPOSE 8000

ENTRYPOINT ["mkdocs"]

CMD ["serve", "--dev-addr=0.0.0.0:8000"]
