This document consists of a *Security Assessment Report (SAR)* for Information System Name (Information System Abbreviation) as required by FedRAMP. This SAR contains the results of the comprehensive security test and evaluation of the Information System Abbreviation system. This assessment report and the results documented herein are provided in support of CSP Name Security Authorization program goals, efforts, and activities necessary to achieve compliance with FedRAMP security requirements. The SAR describes the risks associated with the vulnerabilities identified during the Information System Name security assessment and also serves as the risk summary report as referenced in National Institute of Standards and Technology (NIST) Special Publications (SP) *800-37 Revision 1, Guide for Applying the Risk Management Framework to Federal Information Systems*. 

All assessment results have been analyzed to provide both the information System Owner (SO), CSP Name, and the Authorizing Officials (AOs) with an assessment of the controls that safeguard the confidentiality, integrity, and availability of data hosted by the system as described in the Information System Abbreviation System Security Plan (SSP). 

## APPLICABLE LAWS AND REGULATIONS

The FedRAMP Laws and Regulations can be found on this page: https://www.fedramp.gov/resources/templates-2016/ in the Document Phase SSP attachments.

Table 1 1 Information System Abbreviation Laws and Regulations includes additional laws and regulations specific to Information System Abbreviation. These will include law and regulations from the Federal Information Security Management Act (FISMA), Office of Management and Budget (OMB) circulars, Public Law (PL), United States Code (USC), and Homeland Security Presidential Directives (HSPD). 

> Include any additional Laws and Regulations specific to Information System Abbreviation in the table below.
> 
> Delete this instruction from your final version of this document.

Identification Number | Title | Date | Link
:--|:--|:--|:--
*Reference ID* | *Reference Title* | *Ref Date* | *Reference Link*
*Reference ID* | *Reference Title* | *Ref Date* | *Reference Link*
*Reference ID* | *Reference Title* | *Ref Date* | *Reference Link*

## APPLICABLE STANDARDS AND GUIDANCE

The FedRAMP Standards and Guidance be found on this page: https://www.fedramp.gov/resources/templates-2016/ with the SSP attachments.

Table 1 2 Information System Abbreviation Standards and Guidance includes any additional standards and guidance specific to Information System Abbreviation. These will include standards and guidance from Federal Information Processing Standards (FIPS) and NIST SP.

> Include any additional Standards and Guidance specific to Information System Abbreviation in the table below.
> 
> Delete this instruction from your final version of this document.

Identification Number | Title | Date | Link
:--|:--|:--|:--
*Reference ID* | *Reference Title* | *Ref Date* | *Reference Link*
*Reference ID* | *Reference Title* | *Ref Date* | *Reference Link*
*Reference ID* | *Reference Title* | *Ref Date* | *Reference Link*

## PURPOSE

The purpose of this document is to provide the SO, CSP Name, and the AOs with a SAR for the Information System Abbreviation. A security assessment has been performed on the Information System Abbreviation to evaluate the system’s implementation of, and compliance with, the FedRAMP baseline security controls. The implementation of security controls is described in the SSP, and required by FedRAMP to meet FISMA compliance mandate.

The FedRAMP program requires Cloud Service Providers (CSPs) to use a FedRAMP-accepted Independent Assessor (IA) Third Party Assessment Organization (3PAO) to perform independent security assessment testing and development of the SAR. Security testing for Information System Abbreviation was performed by Third Party Assessment Organization in accordance with the Information System Abbreviation Security Assessment Plan (SAP), dated Date.

## SCOPE

This SAR applies to Information System Abbreviation which is managed and operated by CSP Name. The Information System Abbreviation that is being reported on in this document has a unique identifier which is noted in Table 1-3 Information System Unique Identifier, Name and Abbreviation. 

Unique Identifier | Information System Name | Information System Abbreviation
:--: | :--: | :--:
*Enter FedRAMP Application Number* | Information System Name | Information System Abbreviation

> Instruction: 3PAOs must at the minimum review all the below listed documents. If other documents or files are reviewed, they must be attached in Appendix H and referred to as necessary. 
> 
> Delete this instruction from your final version of this document.

Documentation used by the Third Party Assessment Organization to perform the assessment of Information System Abbreviation includes the following:

* Information System Abbreviation System Security Plan and Attachments
    * Information System Abbreviation Attachment 1: Information Security Policies and Procedures (covering all Control Families)
    * Information System Abbreviation Attachment 2: User Guide
    * Information System Abbreviation Attachment 3: E-Authentication Plan
    * Information System Abbreviation Attachment 4: Privacy Threshold Analysis/Privacy Impact Assessment
    * Information System Abbreviation Attachment 5: Rules of Behavior
    * Information System Abbreviation Attachment 6: Information System Contingency Plan and Test Results
    * Information System Abbreviation Attachment 7: Configuration Management Plan
    * Information System Abbreviation Attachment 8: Incident Response Plan
    * Information System Abbreviation Attachment 9: Control Implementation Summary Report and Worksheet
    * Information System Abbreviation Attachment 10: FIPS-199 Categorization
    * Information System Abbreviation Attachment 11: Separation of Duties Matrix
    * Information System Abbreviation Attachment 12: FedRAMP Laws and Regulations
    * Information System Abbreviation Attachment 13: FedRAMP Inventory Workbook
* Information System Abbreviation Business Impact Analysis
* Information System Abbreviation Security Assessment Plan

The documentation listed above corresponds to the “CSP Security Package Documentation Checklist, dated MM/DD/YYYY, located on the FedRAMP website at the following URL: https://www.fedramp.gov/resources/templates-2016/, under the “Document Phase”. Each system security assessment package must contain the required attachments, as listed.

The Information System Abbreviation is physically located at the facilities noted in Table 1 4 Site Names and Addresses.

Data Center Site Name | Address | Description of Components
:--:|:--:|:--:
? | ? | ?
? | ? | ?
? | ? | ?

> Instruction: 3PAO must ensure that the site names match those found in the IT Contingency Plan (unless the site names found in the IT Contingency Plan were found to be in error in which case that must be noted.) 
> 
> Delete this instruction from your final version of this document.
