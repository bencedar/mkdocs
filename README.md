## Material for MkDocs: Vulnerability Assessment Example

To use...

```bash
docker build -t localhost/mkdocs .
docker run --rm -it -p 8000:8000 -v ${PWD}:/docs localhost/mkdocs
```

Open browser to http://localhost:8000 to view live preview of changes to `mkdocs.yml` and/or items under the `./docs` directory.

The gitlab-ci.yml configuration file for Gitlab CI/CD will use the same dockerfile for building the site and moving the contents over to pages for Gitlab Pages.

To publish...

```bash
docker run --rm -it -v ${PWD}:/docs localhost/mkdocs build
```

You can now find the results in the `./site` directory.

## ToDo

- [ ] Write example markdown files to reflect industry standard Vulnerability Assessment reports.
- [ ] Write custom page css in `docs/stylesheets/printcss` as noted in https://github.com/zhaoterryy/mkdocs-pdf-export-plugin#adjusting-the-output