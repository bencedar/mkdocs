## SECURITY CATEGORIZATION

The Information System Abbreviation is categorized as a \<choose level\> impact system. The Information System Abbreviation categorization was determined in accordance with FIPS 199, Standards for Security Categorization of Federal Information and Information Systems. 

## SYSTEM DESCRIPTION

> Instruction: In the sections below, insert a general description of the information system. Use a description that is consistent with the description found in the System Security Plan (SSP). The description must only differ from the description in the SSP if additional information is going to be included that is not available in the SSP or if the description in the SSP is not accurate. 
> 
> Delete this instruction from your final version of this document.

## PURPOSE OF SYSTEM

> Instruction: In the sections below, insert the purpose of the information system. Ensure that the purpose is consistent with the one in the System Security Plan. 
> 
> Delete this instruction from your final version of this document.
