The assessment methodology used to conduct the security assessment for the Information System Abbreviation system is summarized in the following steps:

1. Perform tests described in the SAP workbook and record the results
2. Identify vulnerabilities related to the CSP platform
3. Identify threats and determine which threats are associated with the cited vulnerabilities
4. Analyze risks based on vulnerabilities and associated threats
5. Recommend corrective actions 
6. Document the results

## PERFORM TESTS

Third Party Assessment Organization performed security tests on the Information System Abbreviation, which were concluded on date. The SAP separately documents the schedule of testing, which was/was not adjusted to provide an opportunity for correcting identified weaknesses and re-validation of those corrections. The results of the tests are recorded in the Security Test Procedures workbooks which are identified in Appendix B Security Test Procedure Workbooks. The findings of the security tests serve as inputs to this SAR. A separate penetration test was performed, with the results documented in a formal Penetration Test Report that is described as an attachment template in Appendix J to this SAR.

### Assessment Deviations

Third Party Assessment Organization performed security tests on the Information System Name and the tests concluded on date. The Table 3 1 List of Assessment Deviations below contains a list of deviations from the original plan for the assessment presented in the SAP.

Deviation ID | Deviation Description | Justification
:--|:--|:--
1 | something | something
2 | something | something
3 | something | something

## IDENTIFICATION OF VULNERABILITIES

Vulnerabilities have been identified by Third Party Assessment Organization for the Information System Abbreviation through security control testing. The results of the security control testing are recorded in the Security Test procedures workbooks and the SAP. 

A vulnerability is an inherent weakness in an information system that can be exploited by a threat or threat agent, resulting in an undesirable impact on the protection of the confidentiality, integrity, or availability of the system (application and associated data). A vulnerability may be due to a design flaw or error in configuration which makes the network, or a host on the network, susceptible to malicious attacks from local or remote users. Vulnerabilities can exist in multiple areas of the system or facilities, such as in firewalls, application servers, web servers, operating systems or fire suppression systems.

Whether or not a vulnerability has the potential to be exploited by a threat depends on a number of variables including (but not limited to):

* The strength of the security controls in place
* The ease at which a human actor could purposefully launch an attack
* The probability of an environmental event or disruption in a given local area

An environmental disruption is usually unique to a geographic location. Depending on the level of the risk exposure, the successful exploitation of a vulnerability can vary from disclosure of information about the host to a complete compromise of the host. Risk exposure to organizational operations can affect the business mission, functions, and/or reputation of the organization.

The vulnerabilities that were identified through security control testing (including penetration testing) for the Information System Abbreviation are identified in the Information System Abbreviation SAR Risk Exposure Table. 

## CONSIDERATION OF THREATS

A threat is an adversarial force or phenomenon that could impact the availability, integrity, or confidentiality of an information system and its networks including the facility that houses the hardware and software. A threat agent is an element that provides the delivery mechanism for a threat. An entity that initiates the launch of a threat agent is referred to as a threat actor.

A threat actor might purposefully launch a threat agent (e.g., a terrorist igniting a bomb). However, a threat actor could also be a trusted employee that acts as an agent by making an unintentional human error (e.g., a trusted staff clicks on a phishing email that downloads malware). Threat agents may also be environmental in nature with no purposeful intent (e.g., a hurricane). Threat agents working alone, or in concert, exploit vulnerabilities to create incidents. FedRAMP categorizes threats using a threat origination taxonomy of Purposeful (P), Unintentional (U), or Environmental (E) type threats as described in Table 3 2 Threat Categories and Type Identifiers.

Threat Origination Category | Type Identifier
:--|:--:
Threats launched puposefully | P
Threats created by unintentional human or machine | U
Threats caused by environmental agents or disruptions | E

Purposeful threats are launched by threat actors for a variety of reasons and the reasons may never be fully known. Threat actors could be motivated by curiosity, monetary gain, political gain, social activism, revenge or many other driving forces. It is possible that some threats could have more than one threat origination category. 

Some threat types are more likely to occur than others. FedRAMP takes threat types into consideration to help determine the likelihood that a vulnerability could be exploited. The threat table shown in Table 3 3 Potential Threats, is designed to offer typical threats to information systems and these threats have been considered for Information System Abbreviation.

> Instruction: A list of potential threats is found in Table 3-3. Assign threat types to vulnerabilities, and then determine the likelihood that a vulnerability could be exploited by the corresponding threat. This table does not include all threat types and the 3PAO may add additional threat types, or modify the listed threats, as needed. 
> 
> Delete this instruction from your final version of this document.

ID | Threat Name | Type Indicator | Description | Confidentiality | Integrity | Availability
:--|:--|:--|:--|:--|:--|:--
T-1 | Alteration | U,P,E | Alteration of data, files, or records. | | Modification | 
T-2 | Audit Compromise | P | An unauthorized user gains access to the audit trail and could cause audit records to be deleted or modified, or prevents future audit records from being recorded, thus masking a security relevant event. |  | Modification or Destruction | Unavailable Accurate Records
T-3 | Bomb | P | An intentional explosion. |  | Modification or Destruction | Denial of Service
T-4 | Communications Failure | U, E | Cut of fiber optic lines, trees falling on telephone lines. |  |  | Denial of Service
T-5 | Compromising Emanations | P | Eavesdropping can occur via electronic media directed against large scale electronic facilities that do not process classified National Security Information. | Disclosure |  | 
T-6 | Cyber Brute Force | P | Unauthorized user could gain access to the information systems by random or systematic guessing of passwords, possibly supported by password cracking utilities.  | Disclosure | Modification or Destruction | Denial of Service
T-7 | Data Disclosure Attack | P | An attacker uses techniques that could result in the disclosure of sensitive information by exploiting weaknesses in the design or configuration. | Disclosure |  | 
T-8 | Data Entry Error | U | Human inattention, lack of knowledge, and failure to cross-check system activities could contribute to errors becoming integrated and ingrained in automated systems. |  | Modification | 
